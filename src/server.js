const express = require ('express')
const ejs = require('ejs')
const pdf = require('html-pdf')
const app = express()
app.use(express.json());

app.post('/', function (request, response) {    
    //const template = Buffer.from(request.body.template, 'base64').toString('utf8')
    const template = request.body.template
    const html = ejs.render(template, {data: request.body.data});
    pdf.create(html).toFile("report.pdf", (err, data) => {
        if(err) {
            return response.send("Erro ao gerar o pdf")
        }
    })
    
    return response.send(html);

})
app.use(express.static('public'))
app.listen(3000)