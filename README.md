O que você vai precisar: 

*nodejs 
*html-pdf
*ejs
*express 

Podemos instalar o Node.js facilmente com o próprio gerenciador de pacotes do Linux. Inicie o terminal pressionando Ctrl + Alt + T.

No terminal, digite o comando de instalação do curl:

sudo apt-get install curl

E por fim, para instalar o Node execute:

sudo apt-get install -y nodejs


Para instalar o ejs: 

npm install ejs

html-pdf: 
npm install -g html-pdf

express: 
npm install express --save



https://github.com/marcbachmann/node-html-pdf